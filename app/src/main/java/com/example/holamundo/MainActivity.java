package com.example.holamundo;

import android.app.Activity;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {
    private Button btnSaludar, btnLimpiar,btnCerrar;
    private TextView lblSaludar;
    private EditText txtNombre;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Relación de los objetos Java con las vistas xml
        btnSaludar = (Button) findViewById(R.id.btnSaludar);
        lblSaludar = (TextView) findViewById(R.id.lblSaludo);
        txtNombre = (EditText) findViewById(R.id.txtSaludo);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);

        //Codificación del boton Saludar
        btnSaludar.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view){
                if(txtNombre.getText().toString().matches("")){
                    //Falta capturar el nombre
                    Toast.makeText(MainActivity.this,"Favor de ingresar el nombre",
                            Toast.LENGTH_SHORT).show();
                }
                else {
                    String saludar = txtNombre.getText().toString();
                    lblSaludar.setText("Hola " + saludar + " Como estas ?");
                }
            }
        });

        //Codificación del boton Limpiar
        btnLimpiar.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view){
                lblSaludar.setText("");
                txtNombre.setText("");
                txtNombre.requestFocus();
            }
        });

        //Codificación del boton cerrar
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
